package com.begodev.arpegy.map;

import java.util.ArrayList;
import java.util.Iterator;

import com.begodev.arpegy.main.Arpegy;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.particles.ParticleSystem;

import com.begodev.arpegy.particle.Effect;

import com.begodev.arpegy.figure.Figure;
import org.newdawn.slick.util.pathfinding.PathFindingContext;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

public class Map implements TileBasedMap {

    public final static int TILESIZE = 16;

    private TiledMapEx map;

    private int layer_ground;
    private int layer_shadow;
    private int layer_walls;
    private int layer_ceil;
    private int layer_objects_bottom;
    private int layer_objects_top;

    public static int renderX;
    public static int renderY;

    private ArrayList<Rectangle> blocks;
    private ArrayList<Portal> portals;
    private ArrayList<Effect> effects;

    private static Map single;

    private Figure hero;

    public synchronized static Map getInstance() throws SlickException {
        if (single == null) {
            single = new Map();
        }

        return single;
    }

    private Map() {
    }

    public void initMap(Figure hero, String mapName, float startPosX,
            float startPosY) throws SlickException {
        this.hero = hero;

        this.map = new TiledMapEx("res/" + mapName + ".tmx", this.hero);
        this.loadLayers();

        renderX = (Arpegy.SCREEN_WIDTH / 2)
                - ((this.map.getWidth() * TILESIZE) / 2);
        renderY = (Arpegy.SCREEN_HEIGHT / 2)
                - ((this.map.getHeight() * TILESIZE) / 2);
        
        this.hero.setPosInTiles(startPosX, startPosY);

        this.blocks = new ArrayList<Rectangle>();
        for (int i = 0; i < map.getObjectCount(0); i++) {
            this.blocks.add(new Rectangle(map.getObjectX(0, i) + Map.renderX,
                    map.getObjectY(0, i) + Map.renderY, map
                    .getObjectWidth(0, i), map.getObjectHeight(0, i)));
        }

        this.portals = new ArrayList<Portal>();
        for (int i = 0; i < map.getObjectCount(1); i++) {
            String nextLocation = map.getObjectProperty(1, i, "to", null);
            float newStartPosX = Float.parseFloat(this.map.getObjectProperty(1,
                    i, "start_position_x", null));
            float newStartPosY = Float.parseFloat(this.map.getObjectProperty(1,
                    i, "start_position_y", null));

            this.portals.add(new Portal(i + 1, nextLocation, newStartPosX,
                    newStartPosY, map.getObjectX(1, i) + Map.renderX, map
                    .getObjectY(1, i) + Map.renderY, map
                    .getObjectWidth(1, i), map.getObjectHeight(1, i)));
        }

        this.effects = new ArrayList<Effect>();
        for (int i = 0; i < map.getObjectCount(2); i++) {
            this.effects.add(new Effect(
                    new Effect.Type(map.getObjectType(2, i)),
                    map.getObjectX(2, i) + renderX,
                    map.getObjectY(2, i) + renderY,
                    map.getObjectWidth(2, i),
                    map.getObjectHeight(2, i)
            )
            );
        }
    }

    private void loadLayers() {
        this.layer_ground = this.map.getLayerIndex("ground");
        this.layer_shadow = this.map.getLayerIndex("shadow");
        this.layer_walls = this.map.getLayerIndex("walls");
        this.layer_ceil = this.map.getLayerIndex("ceil");
        this.layer_objects_bottom = this.map.getLayerIndex("objects_bottom");
        this.layer_objects_top = this.map.getLayerIndex("objects_top");
    }

    public void draw() {
        this.map.render(renderX, renderY, this.layer_ground);
        this.map.render(renderX, renderY, this.layer_shadow);
        this.map.render(renderX, renderY, this.layer_walls);
        this.map.render(renderX, renderY, this.layer_objects_bottom);

        for (Effect effect : this.effects) {
            effect.draw();
        }

        this.hero.draw();

        if (this.layer_objects_top > -1) {
            this.map.render(renderX, renderY, 0, 0, 120, 120, this.layer_objects_top, true);
        }

        this.map.render(renderX, renderY, 0, 0, 120, 120, this.layer_ceil, true);

        // this.effects.get(0);
        // this.map.render(renderX, renderY, this.layer_objects_top);
    }

    public Rectangle[] getBlocks() {
        Rectangle[] rectangles = new Rectangle[this.blocks.size()];

        int i = 0;
        for (Rectangle rect : this.blocks) {
            rectangles[i++] = rect;
        }

        return rectangles;
    }

    public Rectangle[] getPortals() {
        Rectangle[] rectangles = new Rectangle[this.portals.size()];

        int i = 0;
        for (Rectangle rect : this.portals) {
            rectangles[i++] = rect;
        }

        return rectangles;
    }

    public Rectangle[] getEffects() {
        Rectangle[] rectangles = new Rectangle[this.effects.size()];

        int i = 0;
        for (Rectangle rect : this.effects) {
            rectangles[i++] = rect;
        }

        return rectangles;
    }

    public boolean isCollision() throws SlickException {
        for (Rectangle rect : this.blocks) {
            if (this.hero.getHitbox().intersects(rect)) {
                return true;
            }
        }

        return false;
    }

    public Portal enteredPortal() {
        for (Portal portal : this.portals) {
            if (this.hero.getHitbox().intersects(portal)) {
                return portal;
            }
        }
        return null;
    }

    public void updateEffects(int delta) {
        for (Effect effect : this.effects) {
            effect.update(delta);
        }
    }

    @Override
    public int getWidthInTiles() {
        return map.getWidth();
    }

    @Override
    public int getHeightInTiles() {
        return map.getHeight();
    }

    @Override
    public void pathFinderVisited(int i, int i1) {
        
    }

    @Override
    public boolean blocked(PathFindingContext pfc, int x, int y) {
        x = (x * TILESIZE) + renderX;
        y = (y * TILESIZE) + renderY;
        
        Rectangle hitbox = new Rectangle(x+1, y+1, 14, 14);
        
        for (Rectangle rect : this.blocks) {
            if (rect.intersects(hitbox))//(x >= rect.getMinX() && x < rect.getMaxX() && y >= rect.getMinY() && y < rect.getMaxY())
                return true;
        }
        
        return false;
    }

    @Override
    public float getCost(PathFindingContext pfc, int i, int i1) {
        return 1.0f;
    }
}
