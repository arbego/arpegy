package com.begodev.arpegy.map;

import org.newdawn.slick.geom.Rectangle;

public class Portal extends Rectangle {
	private static final long serialVersionUID = -1834608351508582334L;
	private int id;
	private String nextLocation;
	private float startPosX;
	private float startPosY;
	
	public Portal(int id, String nextLocation, float startPosX, float startPosY, float x, float y, float width, float height) {
		super(x, y, width, height);
		this.id = id;
		this.nextLocation = nextLocation;
		
		this.startPosX = startPosX;
		this.startPosY = startPosY;
	}

	public int getId() { 
		return id;
	}

	public String getNextLocation() {
		return this.nextLocation;
	}
	
	public float getStartPosX() {
		return this.startPosX;
	}
	public float getStartPosY() {
		return this.startPosY;
	}
}