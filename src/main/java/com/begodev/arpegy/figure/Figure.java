package com.begodev.arpegy.figure;

import java.util.ArrayList;

import com.begodev.arpegy.map.Map;
import com.begodev.arpegy.map.Portal;
import java.util.LinkedList;
import java.util.Queue;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.Path.Step;

public class Figure {

	private ArrayList<Animation> animations;
	private int currentAnimation = -1;

	private Rectangle collisionBox;
	private int collisionBoxWidth;
	private int collisionBoxHeight;

	private int posX;
	private int posY;

	private Direction currentDirection;
	private boolean isMoving;

        private int posDelta;
        private Queue<Step> path;
        
	public enum Direction {
		UP(0), RIGHT(1), DOWN(2), LEFT(3);

		public int code;

		Direction(int code) {
			this.code = code;
		}
	}

	public Figure() throws SlickException {

                SpriteSheet spriteSheet = new SpriteSheet("res/char.png", 24, 32,
				new Color(32, 156, 0));
		this.collisionBoxWidth = spriteSheet.getSprite(0, 0).getWidth() - 8;
		this.collisionBoxHeight = this.collisionBoxWidth;

		this.animations = new ArrayList<Animation>();
		this.animations.add(new Animation(spriteSheet, 0, 0, 2, 0, true, 100,
				true));
		this.animations.add(new Animation(spriteSheet, 0, 1, 2, 1, true, 100,
				true));
		this.animations.add(new Animation(spriteSheet, 0, 2, 2, 2, true, 100,
				true));
		this.animations.add(new Animation(spriteSheet, 0, 3, 2, 3, true, 100,
				true));
		this.currentAnimation = 0;

                // this.posX = 5 * Map.TILESIZE;
		// this.posY = 6 * Map.TILESIZE;

		this.updateCollisionBox();

		this.setDirection(Direction.UP);
		this.isMoving = false;
                
                this.posDelta = 0;
                this.path = new LinkedList<Step>();
	}

	public void draw() {
		if (this.isMoving
				&& this.animations.get(this.currentAnimation).isStopped())
			this.animations.get(this.currentAnimation).start();

		this.animations.get(this.currentAnimation).draw(posX - 4, posY - 16);
	}

        public void setPath(Path pPath)
        {
            if (pPath != null && pPath.getLength() > 0)
            {
                for (int i=0; i<pPath.getLength(); i++)
                    this.path.add(pPath.getStep(i));
            }
        }
        
	public void move(Direction dir) throws SlickException {
		this.setDirection(dir);
		this.isMoving = true;

		switch (dir) {
		case UP:
			this.posY -= 2;
			break;

		case RIGHT:
			this.posX += 2;
			break;

		case DOWN:
			this.posY += 2;
			break;

		case LEFT:
			this.posX -= 2;
			break;
		}

		this.updateCollisionBox();
		if (Map.getInstance().isCollision()) {
			if (Map.getInstance().enteredPortal() != null) {
				Portal portal = Map.getInstance().enteredPortal();

				System.out.println(portal.getStartPosX() + " "
						+ portal.getStartPosY());
				Map.getInstance().initMap(this, portal.getNextLocation(),
						portal.getStartPosX(), portal.getStartPosY());
			}

			switch (dir) {
			case UP:
				this.posY += 4;
				break;

			case RIGHT:
				this.posX -= 4;
				break;

			case DOWN:
				this.posY -= 4;
				break;

			case LEFT:
				this.posX += 4;
				break;
			}
			this.updateCollisionBox();
		}
	}

	private void updateCollisionBox() {
		if (this.collisionBox == null)
			this.collisionBox = new Rectangle(this.posX + 0, this.posY + 0,
					collisionBoxWidth, collisionBoxHeight);
		else
			this.collisionBox.setLocation(this.posX + 0, this.posY + 0);
	}

	public void stop() {
		this.isMoving = false;
		this.animations.get(this.currentAnimation).stop();
		this.animations.get(this.currentAnimation).setCurrentFrame(1);
	}

	private void setDirection(Direction dir) {
		this.currentDirection = dir;
		this.currentAnimation = dir.code;
	}

	public Rectangle getHitbox() {
		return this.collisionBox;
	}

	public int getPosX() {
		return this.posX;
	}

	public int getPosY() {
		return this.posY;
	}

	public int getPosXInTiles() {
		return (this.posX - Map.renderX) / Map.TILESIZE;
	}

	public int getPosYInTiles() {
		return (this.posY - Map.renderY) / Map.TILESIZE;
	}

	public void setPosInTiles(float x, float y) {
		this.posX = (int) (x * Map.TILESIZE) + Map.renderX;
		this.posY = (int) (y * Map.TILESIZE - Map.TILESIZE) + Map.renderY;
		this.updateCollisionBox();
	}
        
        public void updatePos(int delta)
        {
            this.posDelta += delta;
            
            if (this.posDelta > 50)
            {
                this.posDelta = 0;
                
                if (!this.path.isEmpty())
                {
                    Step step = this.path.poll();
                    if(step != null)
                    {
                        int newX = (step.getX() * Map.TILESIZE) + Map.renderX;
                        int newY = (step.getY() * Map.TILESIZE) + Map.renderY;
                        
                        if (newX > this.posX)
                            this.setDirection(Direction.RIGHT);
                        else if (newX < this.posX)
                            this.setDirection(Direction.LEFT);
                        else if (newY > this.posY)
                            this.setDirection(Direction.DOWN);
                        else if (newY < this.posY)
                            this.setDirection(Direction.UP);
                        
                        this.posX = newX;
                        this.posY = newY;
                        this.updateCollisionBox();
                    }
                }
            }
        }
}
