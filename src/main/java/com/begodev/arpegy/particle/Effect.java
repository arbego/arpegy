package com.begodev.arpegy.particle;

import org.newdawn.slick.geom.Rectangle;
import java.io.File;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;

import com.begodev.arpegy.particle.Effect.Type;

public class Effect extends Rectangle {

	private static final long serialVersionUID = -4653246892445423957L;
	
	private ParticleSystem system;
	private ConfigurableEmitter emitter;

	public static class Type {
		//EFFECT_FIRE("test");
		
		final private String path = "res/particles/";
		private String name;
		
		public Type(String name) {
			this.name = name;
		};
		
		protected String getParticlePath() {
			return this.path + this.name + "_particle.png";
		}
		
		protected String getEmitterPath() {
			return this.path + this.name + "_emitter.xml";
		}
	};
	
	public Effect(Type type, int x, int y, int width, int height) throws SlickException {
		super(x, y, width, height);
		try {
			// load the test particle and
			Image image = new Image(type.getParticlePath(), false);
			system = new ParticleSystem(image, 1500);

			File xmlFile = new File(type.getEmitterPath());
			emitter = ParticleIO.loadEmitter(xmlFile);
			emitter.setPosition(x + (width / 2), y + height, false);
			//emitter.velocity.setActive(true);
			system.addEmitter(emitter);

			system.setBlendingMode(ParticleSystem.BLEND_ADDITIVE);
			
			for (int i=0; i< 100; i++)
			system.update(10);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void draw() {
		this.system.render();
	}
	
	public void update(int delta) {
		this.system.update(delta);
	}
}
