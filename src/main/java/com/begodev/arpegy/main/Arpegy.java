package com.begodev.arpegy.main;

import java.awt.EventQueue;
import java.io.File;

import com.begodev.arpegy.map.Map;
import com.begodev.arpegy.map.TiledMapEx;

import org.lwjgl.Sys;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.InputListener;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;

import com.begodev.arpegy.figure.Figure;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.Path;

public class Arpegy extends BasicGame implements KeyListener {

    private static Arpegy instance;

    private Map map;
    private Figure hero;
    private AStarPathFinder pathFinder;
    
    public static final int SCREEN_WIDTH = 800;
    public static final int SCREEN_HEIGHT = 600;

    public boolean DRAW_BOXES = true;

    public static void main(String[] args) throws SlickException {
        instance = new Arpegy();

        AppGameContainer app;
        app = new AppGameContainer(instance);
        app.setDisplayMode(SCREEN_WIDTH, SCREEN_HEIGHT, false);
        app.setShowFPS(true);
        app.setVSync(true);
        app.start();
        //app.getInput().enableKeyRepeat();
    }

    private Arpegy() {
        super("Test");
    }

    public static Arpegy getInstance() {
        return instance;
    }

    public Figure getHero() {
        return hero;
    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        map.draw();
        // hero.draw();
        //system.render();

        if (DRAW_BOXES) {
            for (Rectangle rect : map.getBlocks()) {
                g.setColor(new Color(255, 255, 255));
                g.draw(rect);
            }

            for (Rectangle rect : map.getPortals()) {
                g.setColor(new Color(255, 0, 255));
                g.draw(rect);
            }

            for (Rectangle rect : map.getEffects()) {
                g.setColor(new Color(255, 0, 0));
                g.draw(rect);
            }

            g.setColor(new Color(0, 255, 0));
            g.draw(hero.getHitbox());
        }

        g.drawString("X:" + ((gc.getInput().getMouseX() - Map.renderX) / Map.TILESIZE) + " Y:" + ((gc.getInput().getMouseY() - Map.renderY) / Map.TILESIZE), 10, 30);
        // g.drawString(emitter.velocity.getMax() + "", 10, 30);//"X: " +
        // gc.getInput().getMouseX() + " Y: " + gc.getInput().getMouseY(), 10,
        // 20);
    }

    @Override
    public void init(GameContainer pgc) throws SlickException {
        hero = new Figure();
        
        map = Map.getInstance();
        map.initMap(hero, "start_1", 10, 12);

        pathFinder = new AStarPathFinder(map, 1000, true);
        //ic = new InputController(gc);
        //instance.setInput(new InputController());
        //System.out.println(ClassLoader.getSystemClassLoader().getResource("res/tilea1.png"));
    }

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        map.updateEffects(delta);
        hero.updatePos(delta);
    }

    @Override
    public void keyPressed(int key, char c) {
        try {
            switch (key) {
                case Input.KEY_UP:
                    Arpegy.getInstance().getHero().move(Figure.Direction.UP);
                    //emitter.setPosition(hero.getPosX()+ 16, hero.getPosY() + 16, false);
                    break;
                case Input.KEY_RIGHT:
                    Arpegy.getInstance().getHero().move(Figure.Direction.RIGHT);
                    //emitter.setPosition(hero.getPosX()+ 16, hero.getPosY() + 16, false);
                    break;
                case Input.KEY_DOWN:
                    Arpegy.getInstance().getHero().move(Figure.Direction.DOWN);
                    //emitter.setPosition(hero.getPosX()+ 16, hero.getPosY() + 16, false);
                    break;
                case Input.KEY_LEFT:
                    Arpegy.getInstance().getHero().move(Figure.Direction.LEFT);
                    //emitter.setPosition(hero.getPosX()+ 16, hero.getPosY() + 16, false);
                    break;

                default:
                    Arpegy.getInstance().getHero().stop();
                    break;
            }
        } catch (SlickException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        
    }
    
    @Override
    public void mousePressed(int button, int x, int y) {
        if (button == Input.MOUSE_RIGHT_BUTTON) {
            System.out.println("source: " + hero.getPosXInTiles() + " " + hero.getPosYInTiles());
            System.out.println("dest: " + Integer.toString((x - Map.renderX) / Map.TILESIZE) + " " + Integer.toString((y - Map.renderY) / Map.TILESIZE));
            //try {
                //System.out.println("blocked: " + Map.getInstance().blocked(null, x, y));
                
                Path path = pathFinder.findPath(null, hero.getPosXInTiles(), hero.getPosYInTiles(), (x - Map.renderX) / Map.TILESIZE, (y - Map.renderY) / Map.TILESIZE);
                
                if (path != null)
                {
                    System.out.println("path length: " + path.getLength());
                    hero.setPath(path);
                    /*for (int i=0; i < path.getLength(); i++)
                    {
                        //System.out.println("step " + i + ": " + path.getX(i) + " " + path.getY(i));
                        hero.setPosInTiles(path.getX(i), path.getY(i));
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Arpegy.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }*/
                }
                else
                    System.out.println("path not found");
                
            //} catch (SlickException ex) {
                //Logger.getLogger(Arpegy.class.getName()).log(Level.SEVERE, null, ex);
            //}
        }
    }
    // KeyListener
}
